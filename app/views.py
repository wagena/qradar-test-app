# Licensed Materials - Property of IBM
# 5725I71-CC011829
# (C) Copyright IBM Corp. 2015, 2020. All Rights Reserved.
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.

import json
from flask import Blueprint, render_template, current_app, send_from_directory, jsonify, request, redirect
from dtdbclient import DTDBClient
from qpylib import qpylib
from utils import get_domaintools_data
from utils import get_iris_products

# pylint: disable=invalid-name
viewsbp = Blueprint('viewsbp', __name__, url_prefix='/')

# A simple "Hello" endpoint that demonstrates use of render_template
# and qpylib logging.
@viewsbp.route('/')
@viewsbp.route('/<name>')
def hello(name=None):
    qpylib.log('name={0}'.format(name), level='INFO')
    return render_template('hello.html', name=name)

@viewsbp.route('/settings')
def view_settings():
    dbclient = DTDBClient()
    settings = dbclient.get_dt_settings()

    return render_template("settings.html", form=settings)

@viewsbp.route('/settings', methods=["POST"])
def submit_form():
    qpylib.log('wes test settings: {0}'.format(json.dumps(request.form)))
    qpylib.log('wes test api key: {0}'.format(request.form['api_key']))

    if 'test_connection' in request.form:
        message = test_connection(request.form)
    else:
        message = save_settings(request.form)

    return render_template("settings.html", form=request.form, message=message)

def test_connection(settings):
    dt_params = {"endpoint": "account-information"}
    result = get_domaintools_data(dt_params, settings)
    qpylib.log('wes test api response: {0}'.format(json.dumps(result)))

    if 'error' in result:
        qpylib.log('wes test api result error')
        return {'type': 'danger', 'text': result['error']}

    iris_products = get_iris_products(result.get('response', {}))
    qpylib.log('wes test iris products: {0}'.format(json.dumps(iris_products)))
    if len(iris_products) < 1:
        qpylib.log('wes test api result no iris')
        return {'type': 'danger', 'text': 'This account does not have any Iris products, use a username/api key which has Iris products'}

    return {'type': 'success', 'text': 'Connection Successful!'}

def save_settings(settings):
    dbclient = DTDBClient()
    dbclient.update_dt_settings(settings)

    return {'type': 'success', 'text': 'Save Successful!'}


# The presence of this endpoint avoids a Flask error being logged when a browser
# makes a favicon.ico request. It demonstrates use of send_from_directory
# and current_app.
@viewsbp.route('/favicon.ico')
def favicon():
    return send_from_directory(current_app.static_folder, 'favicon-16x16.png')
