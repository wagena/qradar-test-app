__author__ = 'DomainTools'

from qpylib import qpylib
from flask import Flask
import os.path
from contextlib import closing
import sqlite3


class DTDBClient:

    def __init__(self):
        self.DATABASE = qpylib.get_store_path("dtstore.db")
        self.app = Flask(__name__)

        self.init_db()
        self.run_migrations()

        self.valid_settings = [
            'api_user',
            'api_key',
            'enable_custom_certificate',
            'custom_certificate_path',
            'enable_proxy',
            'proxy_port',
            'proxy_server'
        ]

        self.boolean_settings = [
            'enable_custom_certificate',
            'enable_proxy'
        ]

    def connect_db(self):
        return sqlite3.connect(self.DATABASE)

    def init_db(self, bForce=False):
        if not os.path.isfile(self.DATABASE) or bForce is True:
            with closing(self.connect_db()) as db:
                with self.app.open_resource('schema/dtschema.sql',
                                            mode='r') as f:
                    db.cursor().executescript(f.read())
                db.commit()
    
    def run_migrations(self):
        if os.path.isfile(self.DATABASE):
            with closing(self.connect_db()) as db:
                with self.app.open_resource('schema/migrations.sql',
                                            mode='r') as f:
                    db.cursor().executescript(f.read())
                db.commit()

    def update_dt_settings(self, updated_settings):
        db = self.connect_db()

        values = tuple([updated_settings.get(setting, '') for setting in self.valid_settings])
        query = 'REPLACE INTO dt_settings(id, api_user, api_key, enable_custom_certificate, custom_certificate_path, enable_proxy, proxy_port, proxy_server, updated_date) VALUES(1, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP);'

        db.execute(query, values)
        db.commit()
        if db is not None:
            db.close()

    def get_dt_settings(self):
        db = self.connect_db()
        db.row_factory = sqlite3.Row
        result = db.execute('SELECT * FROM dt_settings WHERE id=1')

        settings = {}
        result = result.fetchone()

        if result:
            for key in self.valid_settings:
                settings[key] = bool(result[key]) if key in self.boolean_settings else result[key]

        if db is not None:
            db.row_factory = None
            db.close()

        return settings

