-- DROP TABLE IF EXISTS dt_settings;

CREATE TABLE IF NOT EXISTS dt_settings (
  id INTEGER PRIMARY KEY autoincrement,
  api_user TEXT,
  api_key TEXT,
  enable_custom_certificate INTEGER,
  custom_certificate_path TEXT,
  enable_proxy INTEGER,
  proxy_port TEXT,
  proxy_server TEXT,
  updated_date TIMESTAMP NOT NULL
);