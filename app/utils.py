from domaintools import API
from domaintools import __version__ as dt_api_version
from domaintools.exceptions import (
    ServiceException,
    NotAuthorizedException,
    ServiceUnavailableException,
)
import requests

def get_domaintools_data(dt_params, dt_config):
    """
    Method for talking to DT.
    dt_params fields dependent on if this is from testing the connection or querying domain data.
    :param dt_params: {
                       "endpoint": "iris-enrich",
                       "domains": "domaintools.com,int-chase.com"
                      }
    :param es_config: {}
    :param dt_config: {
                       "enable_proxy": False,
                       "proxy_server": "localhost",
                       "proxy_port": "8000"
                       "enable_custom_certificate": False,
                       "custom_certificate_path": "path",
                       "api_key": "some-key",
                       "api_user": "username"
                      }
    :return:
    """
    try:
        if dt_config.get("enable_proxy", False):
            proxy = "{}:{}".format(
                dt_config.get("proxy_server"), dt_config.get("proxy_port")
            )
        else:
            proxy = None

        if dt_config.get("enable_custom_certificate", False):
            verify_ssl = dt_config.get("custom_certificate_path", "")
        else:
            verify_ssl = True

        api = API(
            username=dt_config["api_user"],
            key=dt_config["api_key"],
            https=False,
            verify_ssl=verify_ssl,
            proxy_url=proxy,
            always_sign_api_key=True,
            app_partner="elastic",
            app_name="domaintoolsEnrich",
            app_version="1.0",
            api_version=dt_api_version,
        )
        endpoint = dt_params.get("endpoint")
        if endpoint == "account-information":
            return api.account_information().data()
        elif endpoint == "iris-enrich":
            return api.iris_enrich(dt_params.get("domains")).data()
        elif endpoint == "iris-investigate":
            return api.iris_investigate(dt_params.get("domains")).data()
    except requests.exceptions.ProxyError as e:
        return {'error': 'Unable to connect to proxy. Please check your proxy settings and try again.'}
    except requests.exceptions.SSLError as e:
        return {'error': 'Invalid SSL Certificate. Please check your SSL settings and try again.'}
    except NotAuthorizedException as e:
        return {'error': 'Invalid DomainTools API Username/Key. Please reach out to enterprisesupport@domaintools.com if you need API keys.'}
    except ServiceUnavailableException as e:
        return {'error': 'You may have exhausted the allocated API/ Query limits. We may need to adjust your API Query/ Rate Limits if you continue to face this issue. Please reach out to enterprisesupport@domaintools.com'}
    except Exception as e:
        return {'error': str(e)}

def get_iris_products(response):
    products = response.get('products', [])

    return list(filter(lambda x: x['id'] == 'iris-investigate' or x['id'] == 'iris-enrich', products))